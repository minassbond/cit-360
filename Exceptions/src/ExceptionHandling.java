import java.util.*;
public class ExceptionHandling {
    public static void main(String[] args) {
        //initiate input
        Scanner in = new Scanner(System.in);
        boolean retry = false;
        //retry loop
        while (!retry) {
            try {
                System.out.println("Please enter 2 numbers that we are going to divide: ");
                //number input
                float numer = in.nextFloat();
                int demon = in.nextInt();
                System.out.println("You entered " + numer + " and " + demon);
                if (demon == 0) {
                    throw new ArithmeticException();
                } else {
                    float divide = numer / demon;
                    System.out.println("You're answer is " + divide);
                    retry = true;
                }
            } catch (ArithmeticException e) {
                System.out.println("You've tried to destroy the universe by dividing by zero.  Please don't and try again.");
            }
        }
    }
}

