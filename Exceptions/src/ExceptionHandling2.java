import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling2 {
    public static void main(String[] args) {
        //initiate input
        Scanner in = new Scanner(System.in);
        boolean retry = false;
        //retry loop
        while (!retry) {
            System.out.println("Please enter 2 numbers that we are going to divide: ");
            //number input
            String numer = in.next();
            String demon = in.next();
            try{
                int inputNumer = Integer.parseInt(numer);
                int inputDemon = Integer.parseInt(demon);
            if (inputDemon == 0) {
                throw new ArithmeticException();
            } else {
                float divide = inputNumer / inputDemon;
                System.out.println("You're answer is " + divide);
                retry = true;
            }
        }catch(ArithmeticException e){
                System.out.println("You've tried to destroy the universe by dividing by zero.  Please don't and try again.");
            }catch(NumberFormatException e){
                System.out.println("Please try again, enter a number this time. ");
            }
        }
    }
    }

