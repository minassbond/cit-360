import java.sql.Time;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class AtomicCounter{
    private AtomicInteger i = new AtomicInteger(0);

    public int incrementAndGet(){
        return i.incrementAndGet();
    }
    public int getCount(){
        return i.get();
    }
}

public class BattleStart {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService activeBattle = Executors.newFixedThreadPool(2);

        AtomicCounter atom = new AtomicCounter();

        PokemonBattle b4 = new PokemonBattle("Squirtle", 25, 30);
        PokemonBattle b3 = new PokemonBattle("Blastoise", 75, 500);
        PokemonBattle b2 = new PokemonBattle("Dragonite", 50, 750);
        PokemonBattle b1 = new PokemonBattle("Metagross", 100, 900);

        try {
            for (int i = 0; i < 10; i++) {
                activeBattle.submit(() -> atom.incrementAndGet());

                activeBattle.execute(b1);
                activeBattle.execute(b2);
                activeBattle.execute(b3);
                activeBattle.execute(b4);
            }
            activeBattle.shutdown();
            activeBattle.awaitTermination(60, TimeUnit.SECONDS);
            System.out.println("Final Count: " + atom.getCount());
        }catch(Exception e){
            System.out.println("Please check you threads and runnables, I've got a bad feeling about this");
        }
    }
}
