import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PokemonBattle implements Runnable {

    private String pokemon;
    private int hp;
    private int attack;
    private int rand;
    private Object i;

    public class atom{
        AtomicInteger i;

        public void incrementNumber(){
            i.incrementAndGet();
        }
    }

    public PokemonBattle(String pokemon, int hp, int attack){

        this.pokemon = pokemon;
        this.hp = hp;
        this.attack = attack;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        try {
            System.out.println("Let the battle being!  Pokemon: " + pokemon + " HP: " + hp + " Attack = " + attack + " Damage = " + rand + "\n");
            for (int i = 1; i < rand; i++) {
                if (i % hp == 0) {
                    System.out.print(pokemon + " has been hit! ");
                    try {
                        Thread.sleep(attack);
                    } catch (InterruptedException e) {
                        System.err.println(e.toString());
                    }
                    System.out.println("\n" + pokemon + " has flinched!\n");
                }
            }
        }catch(Exception e){
            System.out.println("Something is wrong with your runnable set up.  Try again!");
        }
    }
}
