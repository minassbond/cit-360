package mike.minassian;

public class Loadout {

    private String mainGun;
    private String sideArm;

    public String getMainGun(){
        return mainGun;
    }

    public void setMainGun(String mainGun){
        this.mainGun = mainGun;
    }

    public String getSideArm(){
        return sideArm;
    }

    public void setSideArm(String sideArm){
        this.sideArm = sideArm;
    }

    public String toString(){
        return "Main Weapon: " + mainGun + " Sidearm: " + sideArm;
    }
}
