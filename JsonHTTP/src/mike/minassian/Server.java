package mike.minassian;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) throws IOException {



        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/loadout", new MyHandler());
        server.setExecutor(null);
        System.out.println("Server is active!");
        server.start();
    }
    static class MyHandler implements HttpHandler{
        public void handle(HttpExchange t) throws IOException {
            Loadout start = new Loadout();
            start.setMainGun("Assault Rifle");
            start.setSideArm("Magnum Pistol");
            String s = JSON.loadoutToJSON(start);
        byte [] json = s.getBytes();
        t.sendResponseHeaders(200, json.length);
        OutputStream os = t.getResponseBody();
        os.write(json);
        os.close();
    }
        public static String loadoutToJSON(Loadout loadout) {

            ObjectMapper bag = new ObjectMapper();
            String s = "";
            try {
                s = bag.writeValueAsString(loadout);
            } catch (JsonProcessingException e) {
                System.err.println(e.toString());
            }
            return s;
        }

    }
}
