package mike.minassian;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.*;
import java.net.*;
import java.nio.Buffer;

public class Client {

    private static HttpURLConnection connection;

    public static void main(String[] args) throws IOException {
        BufferedReader reader;
        String line;
        StringBuffer responseContent = new StringBuffer();


        URL url = new URL("http://localhost:8000/loadout");
        connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while((responseLine = br.readLine()) !=null){
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        } catch (MalformedURLException e) {
            System.out.println("URL may not be correct, I think you should look again.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

