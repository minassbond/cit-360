package mike.minassian;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class JSON {

    public static String loadoutToJSON(Loadout loadout) {

        ObjectMapper bag = new ObjectMapper();
        String s = "";
        try {
            s = bag.writeValueAsString(loadout);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }

    public static Loadout JSONtoLoadout(String s){

        ObjectMapper bag = new ObjectMapper();
        Loadout loadout = null;

        try {
                loadout = bag.readValue(s, Loadout.class);
        }catch (IOException e) {
            System.err.println(e.toString());
        }
        return loadout;
    }

    public static void main(String[] args) {

        Loadout start = new Loadout();
        start.setMainGun("Assault Rifle");
        start.setSideArm("Magnum Pistol");

        String json = JSON.loadoutToJSON(start);
        System.out.println(json);

        Loadout nxtlvl = JSON.JSONtoLoadout(json);
        System.out.println(nxtlvl);
    }
}
