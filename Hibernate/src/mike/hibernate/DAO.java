package mike.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DAO {

    SessionFactory factory = null;
    Session ses1 = null;

    private static DAO single_instance=null;

    private DAO(){
        factory = HibernateUtility.getSessionFactory();
    }

    public static DAO getInstance(){
        {
            if (single_instance == null){
                single_instance = new DAO();
            }
            return single_instance;
        }
    }

    public List<Pokemon> getPokemon() {

        try{
            ses1 = factory.openSession();
            ses1.getTransaction().begin();
            String sql = "from mike.hibernate.Pokemon";
            List<Pokemon> pk = (List<Pokemon>)ses1.createQuery(sql).getResultList();
            ses1.getTransaction().commit();
            return pk;
        }catch (Exception e){
            e.printStackTrace();
            //Session rollback
            ses1.getTransaction().rollback();
            return null;
        } finally{
            ses1.close();
        }
    }

    public Pokemon getPokemon(int pokeID){

        try{
            ses1 = factory.openSession();
            ses1.getTransaction().begin();
            String sql = "from mike.hibernate.Pokemon where id=" + Integer.toString(pokeID);
            Pokemon p = (Pokemon)ses1.createQuery(sql).getSingleResult();
            ses1.getTransaction().commit();
            return p;
        }catch (Exception e){
            e.printStackTrace();
            //Rollback
            ses1.getTransaction().rollback();
            return null;
        }finally{
            ses1.close();
        }
    }

}
