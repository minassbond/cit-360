package mike.hibernate;
import java.util.*;


public class RunPokemon {

    public static void main(String[] args) {

        DAO d = DAO.getInstance();

        List<Pokemon> pk = d.getPokemon();
        try {
            for (Pokemon i : pk) {
                System.out.println(i);
            }
            System.out.println(d.getPokemon(25));
        } catch (Exception e) {
            System.out.println("Something is wrong with DAO or Hibernate connection. Please review code logs");
        }
    }
}

