import javax.swing.*;
import java.util.*;

public class TempConversion {

    public double tempConversion (double temperature, String units){
        if(units.equals("F")) {
            return (temperature - 32) * (5.0 / 9.0);
        }else{
            return (temperature * (9.0 / 5.0)) + 32;
        }
    }

}
