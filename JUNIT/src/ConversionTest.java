import org.junit.*;
import org.junit.Test;
import java.util.*;

public class ConversionTest {
    @After
    public void OtherTest () throws Throwable{
        TempConversion otherTest = new TempConversion();
        double temperature = 100;
        String units = "C";
        double result = otherTest.tempConversion(temperature, units);
        System.out.println(result);
    }

    @Test
    public void ConversionTest() throws Throwable {

        TempConversion underTest = new TempConversion();

        double temperature = 32.0;
        String units = "F";
        double result = underTest.tempConversion(temperature, units);

        System.out.println(result);
    }
    @Before
    public void NegativeTest() throws Throwable {
        TempConversion negTest = new TempConversion();
        double temperature = -15;
        String units = "C";
        double result = negTest.tempConversion(temperature, units);

        System.out.println(result);
    }

    public static void main(String[] args) {
        Boolean retry = false;
        while(!retry){
            TempConversion underTest = new TempConversion();
            Scanner in = new Scanner(System.in);
            System.out.println("Please enter a temperature followed by F or C: ");
            try {
                double temperature = in.nextDouble();
                String units = in.next();
                double result = underTest.tempConversion(temperature, units);
                System.out.println(result);
                retry = true;
            } catch (Exception e) {
                System.out.println("Something is wrong with your enter values, don't screw up next time:");
            }
        }
    }

}
