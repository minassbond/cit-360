import java.util.*;
public class CollectionsHW {
    public static void main(String[] args) {
        //Demonstrate list
        System.out.println("Video Game list:\n");
        List<String> game_list = new ArrayList();
        game_list.add("Halo 2");
        game_list.add("Pokemon: Red Version");
        game_list.add("Star Wars: Battlefront 2");
        game_list.add("Super Smash Bros. Ultimate");
        game_list.add("Mario Kart 8");
        game_list.add("Super Mario Odyssey");
        try {
            //Print out list
            System.out.println(game_list);
            //Swap elements 1 and 2 of the list
            game_list.set(1, "Star Wars: Battlefront 2");
            game_list.set(2, "Pokemon: Red Version");
            //Print out list
            System.out.println(game_list);
            System.out.println();
            //Move through list unsorted
            for (int i=0; i<game_list.size(); i++)
                System.out.println(game_list.get(i));
            //Move through list sorted
            Collections.sort(game_list);
            System.out.println();
            for (int i=0; i<game_list.size(); i++)
                System.out.println(game_list.get(i));
        } catch (Exception ex) {
            //Issue with the running of the ArrayList test
            System.out.println("There is an error with the Array List, double check the display code is set properly");
        }

        //Demonstrate Set
        System.out.println("\nWeapons Backpack:\n");
        Set weapon_drops = new TreeSet();
        weapon_drops.add("Assault Rifle");
        weapon_drops.add("Magnum Pistol");
        weapon_drops.add("Plasma Rifle");
        weapon_drops.add("Sniper Rifle");
        weapon_drops.add("Magnum Pistol");
        weapon_drops.add("Needler");
        weapon_drops.add("Plasma Pistol");
        weapon_drops.add("Plasma Pistol");
        weapon_drops.add("Plasma Pistol");

        try {
            System.out.println(weapon_drops);
            System.out.println("\nDrop Plasma Pistol from backpack\n");
            //drop Plasma pistol from Treeset
            weapon_drops.remove("Plasma Pistol");
            //Iterate through backpack
            Iterator backpack = weapon_drops.iterator();
            while (backpack.hasNext()) {
                System.out.println(backpack.next());
            }
        } catch(Exception ex){
            System.out.println("There is an error within the iterator format.  Please double check the code");
        }

        //Demonstrate Queue
        System.out.println("\nPokemon bag Resources\n");
        Queue health_items = new PriorityQueue();
        health_items.add("Potion");
        health_items.add("Super potion");
        health_items.add("Full Heal");
        health_items.add("Full Restore");
        health_items.add("Burn Heal");
        health_items.add("Full Heal");
        health_items.add("Black Sludge");
        health_items.add("Leftovers");
        try {
            //iterator creation
            Iterator healing = health_items.iterator();
            //look at first item in queue
            System.out.println(health_items.peek() + "\n");
            //pull first item in queue
            System.out.println(health_items.poll() + "\n");
            //look at new first item in queue
            System.out.println(health_items.peek() + "\n");
            //iterate through rest of queue
            while (healing.hasNext()) {
                System.out.println(health_items.poll());
            }
            //look at new first item in queue
            System.out.println("\n" + health_items.peek());
        } catch(Exception ex){
            System.out.println("There is an error when trying to look into the queue.  Please make sure the .peeks and .polls are correct");
        }

        //Demonstrate Map
        System.out.println("\nList of Legendary, Mythical and Sudo-legendary Pokemon:\n");
        Map<String, String> pokemon = new HashMap();
        pokemon.put("legendary Red", "Mewtwo");
        pokemon.put("legendary Gold", "Ho-oh");
        pokemon.put("mythical Red", "Mew");
        pokemon.put("legendary Silver", "Lugia");
        pokemon.put("legendary Ruby", "Groudon");
        pokemon.put("legendary Sapphire", "Kyogre");
        pokemon.put("legendary Emerald", "Raquaza");
        pokemon.put("mythical Blue", "Moltres");
        pokemon.put("mythical Blue", "Zapdos");
        pokemon.put("sudo 1st Gen", "Dragonite");
        pokemon.put("sudo 4th Gen", "Metagross");
        pokemon.put("sudo 5th Gen", "Tyranitar");
        try {
            //display number of items in map
            System.out.println("Map of Pokemon contains " + pokemon.size() + " mappings.");
            //use loop to print through map
            for (Map.Entry<String, String> e : pokemon.entrySet())
                System.out.println("Key: " + e.getKey() + " Value: " + e.getValue());
        } catch(Exception ex){
            System.out.println("Error when trying to move through hashmap, please double check the for loop");
        }
    }
}
